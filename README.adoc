= LabVIEW Toolkit Manual
:toc: 

Tool for automatic documentation of LabVIEW toolkit.

NOTE: This project is an add-on to https://gitlab.com/wovalab/open-source/labview-doc-generator[Antidoc]

== Add-on goals

Generate documentation of a Toolkit.

NOTE: In this project, LabVIEW toolkitstands for a set of VI with accessible trhoug a palette in LabVIEW